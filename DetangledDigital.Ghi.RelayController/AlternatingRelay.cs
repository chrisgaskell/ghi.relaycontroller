using System;
using Microsoft.SPOT;
using Gadgeteer.Modules.GHIElectronics;
using Gadgeteer;

namespace DetangledDigital.Ghi.RelayController
{
    public class AlternatingRelay
    {
        private readonly Relay_X1 _relay;
        private readonly MulticolorLed _multicolorLed;
        bool Relay_Is_On = false;

        public AlternatingRelay(Relay_X1 relay, MulticolorLed multicolorLed)
        {
            _relay = relay;
            _multicolorLed = multicolorLed;
            _multicolorLed.TurnBlue();
        }

        public void Flash(Timer timer)
        {
            Alternate();
        }

        public void Alternate()
        {
            if (Relay_Is_On)
            {
                _multicolorLed.TurnGreen();
                _relay.TurnOff();
                Debug.Print("Relay is off");
            }
            else
            {
                _multicolorLed.TurnRed();
                _relay.TurnOn();
                Debug.Print("Relay is on");
            }

            Relay_Is_On = !Relay_Is_On;
        }
    }
}
